## qb_move (noetic) - 3.0.3-2

The packages in the `qb_move` repository were released into the `noetic` distro by running `/usr/bin/bloom-release qb_move --track noetic --rosdistro noetic --edit` on `Wed, 26 Jul 2023 13:52:53 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.2.2-1`
- old version: `3.0.3-1`
- new version: `3.0.3-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## qb_move (noetic) - 3.0.3-1

The packages in the `qb_move` repository were released into the `noetic` distro by running `/usr/bin/bloom-release qb_move --track noetic --rosdistro noetic --edit` on `Wed, 26 Jul 2023 13:37:59 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-1`
- new version: `3.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## qb_move (melodic) - 3.0.0-1

The packages in the `qb_move` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_move --track melodic --rosdistro melodic` on `Mon, 09 Jan 2023 10:14:17 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.0.0-1`
- old version: `2.2.1-1`
- new version: `3.0.0-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_move (noetic) - 2.2.2-1

The packages in the `qb_move` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_move --track noetic --rosdistro noetic` on `Wed, 01 Sep 2021 15:37:28 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.2.1-1`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_move (noetic) - 2.2.1-1

The packages in the `qb_move` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_move --track noetic --rosdistro noetic --new-track` on `Mon, 30 Aug 2021 10:03:44 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_move (melodic) - 2.2.1-1

The packages in the `qb_move` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_move --track melodic --rosdistro melodic` on `Mon, 30 Aug 2021 09:54:28 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_gazebo`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.0.0-1`
- old version: `2.0.0-1`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_move (kinetic) - 2.1.3-1

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_move --track kinetic --rosdistro kinetic` on `Mon, 07 Oct 2019 10:32:09 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.1.2-1`
- old version: `2.1.2-1`
- new version: `2.1.3-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.16.1`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.42`


## qb_move (kinetic) - 2.1.2-1

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_move --track kinetic --rosdistro kinetic` on `Wed, 12 Jun 2019 07:14:06 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.1.0-1`
- old version: `2.1.0-1`
- new version: `2.1.2-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## qb_move (kinetic) - 2.1.0-1

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_move --track kinetic --rosdistro kinetic` on `Tue, 28 May 2019 16:09:46 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.1.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## qb_move (melodic) - 2.0.0-1

The packages in the `qb_move` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_move --track melodic --rosdistro melodic` on `Fri, 01 Jun 2018 13:25:49 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.0.0-0`
- new version: `2.0.0-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_move (lunar) - 2.0.0-1

The packages in the `qb_move` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_move --track lunar --rosdistro lunar` on `Fri, 01 Jun 2018 13:17:43 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.0.0-0`
- new version: `2.0.0-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_move (melodic) - 2.0.0-0

The packages in the `qb_move` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_move --track melodic --rosdistro melodic --new-track` on `Thu, 31 May 2018 15:25:37 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_move (lunar) - 2.0.0-0

The packages in the `qb_move` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_move --track lunar --rosdistro lunar --new-track` on `Thu, 31 May 2018 15:12:12 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_move (kinetic) - 2.0.0-0

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_move --track kinetic --rosdistro kinetic` on `Thu, 31 May 2018 10:26:35 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `1.0.6-0`
- old version: `1.0.6-0`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_move (kinetic) - 1.0.6-0

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_move` on `Fri, 24 Nov 2017 11:57:24 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `1.0.5-0`
- old version: `1.0.5-0`
- new version: `1.0.6-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_move (kinetic) - 1.0.5-0

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_move` on `Tue, 27 Jun 2017 13:22:49 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `1.0.4-0`
- old version: `1.0.4-0`
- new version: `1.0.5-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_move (kinetic) - 1.0.4-0

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_move` on `Fri, 23 Jun 2017 16:05:14 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbmove-ros-release.git
- rosdistro version: `1.0.1-0`
- old version: `1.0.1-0`
- new version: `1.0.4-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_move (kinetic) - 1.0.1-0

The packages in the `qb_move` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_move --edit` on `Mon, 19 Jun 2017 15:57:31 -0000`

These packages were released:
- `qb_move`
- `qb_move_control`
- `qb_move_description`
- `qb_move_hardware_interface`

Version of package(s) in repository `qb_move`:

- upstream repository: https://bitbucket.org/qbrobotics/qbmove-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


